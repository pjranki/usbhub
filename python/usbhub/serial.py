import os
import sys
import time
import serial
import serial.tools.miniterm
import uuid


class SerialPopenInstance(object):

    def __init__(self, serial, args):
        self.serial = serial
        self.args = args
        assert self.args[0].startswith('adb')
        index = self.args.index('shell')
        assert index > 0
        self.args = self.args[index + 1:]
        self.stdout = self.serial.shell(self.args)
        self.stderr = b''

    def communicate(self, *_args, **_kwargs):
        return self.stdout, self.stderr

    @property
    def returncode(self):
        return 0


class Serial(object):

    def __init__(self, port, baud):
        self.ser = serial.Serial(port, baud, timeout=0)
        self.input_data = b''

    def __call__(self, args, *_args, **kwargs):
        return SerialPopenInstance(self, args)

    def write(self, data):
        assert isinstance(data, bytes)
        while len(data) > 0:
            written = self.ser.write(data)
            assert written >= 0
            data = data[written:]
        return len(data)

    def read(self, size):
        while True:
            recd = self.ser.read(100)
            if len(recd) == 0:
                break
            self.input_data += recd
        data = self.input_data[:size]
        self.input_data = self.input_data[size:]
        return data

    def readline(self):
        while True:
            recd = self.ser.read(100)
            if len(recd) == 0:
                break
            self.input_data += recd
        index_1 = self.input_data.find(b'\r\r\n')
        index_2 = self.input_data.find(b'\r\n')
        index_3 = self.input_data.find(b'\n')
        if index_1 >= 0:
            line = self.input_data[:index_1]
            self.input_data = self.input_data[index_1 + 3:]
        elif index_2 >= 0:
            line = self.input_data[:index_2]
            self.input_data = self.input_data[index_2 + 2:]
        elif index_3 >= 0:
            line = self.input_data[:index_3]
            self.input_data = self.input_data[index_3 + 1:]
        else:
            line = None
        if line:
            line = line.decode()
        return line

    def readline(self):
        while True:
            recd = self.ser.read(100)
            if len(recd) == 0:
                break
            self.input_data += recd
        index_1 = self.input_data.find(b'\r\r\n')
        index_2 = self.input_data.find(b'\r\n')
        index_3 = self.input_data.find(b'\n')
        if index_1 >= 0:
            line = self.input_data[:index_1]
            self.input_data = self.input_data[index_1 + 3:]
        elif index_2 >= 0:
            line = self.input_data[:index_2]
            self.input_data = self.input_data[index_2 + 2:]
        elif index_3 >= 0:
            line = self.input_data[:index_3]
            self.input_data = self.input_data[index_3 + 1:]
        else:
            line = None
        return line

    def shell(self, args=None):
        if isinstance(args, str):
            args = args.split(' ')
        args = list() if not args else args
        no_args = len(args) == 0
        if no_args:
            miniterm = serial.tools.miniterm.Miniterm(self.ser)
            miniterm.exit_character = chr(0x1d)
            miniterm.set_rx_encoding('UTF-8')
            miniterm.set_tx_encoding('UTF-8')
            miniterm.start()
            try:
                miniterm.join(True)
            except KeyboardInterrupt:
                pass
            miniterm.join()
            miniterm.close()
            return

        end_marker = str(uuid.uuid4())
        command = ' '.join(args)
        self.ser.write('{}\necho {}\n'.format(command, end_marker).encode('ascii'))
        timestamp_last_stdout = time.time()
        stdout = b''
        capture_stdout = False
        while True:
            line = self.readline()
            if line:
                if line == command.encode('ascii'):
                    capture_stdout = True
                elif line.strip().startswith(b'['):
                    pass
                elif line.find(end_marker.encode('ascii')) != -1:
                    if line.find(b'echo ') == -1:
                        break
                elif capture_stdout:
                    timestamp_last_stdout = time.time()
                    stdout += line + b'\n'
                delta = time.time() - timestamp_last_stdout
                if delta > 1.0:
                    break
        return stdout
