import os
import sys
import subprocess


class Adb(object):

    def __init__(self, bin=None, Popen=None, root=False):
        self._bin = bin if bin else ('adb.exe' if os.name == 'nt' else 'adb')
        self._Popen = Popen if Popen else subprocess.Popen
        self._root = root

    def Popen(self, args, *_args, **_kwargs):
        new_args = [self._bin]
        new_args.extend(args)
        args = new_args
        return self._Popen(args, *_args, **_kwargs)

    def devices(self):
        args = [self._bin, 'devices']
        p = self._Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        device_list = list()
        list_of_devices_found = False
        for line in stdout.decode().split('\n'):
            line = line.strip()
            line = line.replace('\t', ' ')
            while line.find('  ') != -1:
                line = line.replace('  ', ' ')
            if list_of_devices_found:
                items = line.split(' ')
                if len(items) >= 2:
                    device_list.append((items[0], items[1]))
            elif line.startswith('List of devices'):
                list_of_devices_found = True
        return device_list

    def shell(self, args=None):
        if isinstance(args, str):
            args = args.split(' ')
        args = list() if not args else args
        no_args = len(args) == 0
        new_args = ['shell']
        new_args.extend(args)
        args = new_args
        if no_args:
            p = self.Popen(args)
        else:
            p = self.Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        if not stdout:
            stdout = b''
        if not stderr:
            stderr = b''
        return p.returncode, stdout.decode(), stderr.decode()

    def unlock(self):
        self.shell('input keyevent 82')

    def back(self):
        self.shell('input keyevent KEYCODE_BACK')

    def home(self):
        self.shell('input keyevent KEYCODE_HOME')

    def reboot(self):
        self.shell('reboot')

    def power_off(self):
        self.shell('reboot -p')

    def getprop(self, name):
        returncode, stdout, _ = self.shell('getprop {}'.format(name))
        assert returncode == 0
        return stdout.split('\n')[0].strip()

    @property
    def boot_completed(self):
        return self.getprop('sys.boot_completed') == '1'
