#include <esptinyusb.h>
#include <hidcomposite.h>
#include <hidgamepad.h>
#include <hidgeneric.h>
#include <hidkeyboard.h>
#include <hidkeylayout.h>
#include <hidmouse.h>
#include <hidusb.h>

#include <Wire.h>

// Arduino library EspTinyUSB
#include "hidcomposite.h"

#if CFG_TUD_HID
HIDcomposite device;

#define CMD_TINY_USB_HID_KEY    (0x01)
#define CMD_TYPE_STRING         (0x02)
#define CMD_SCROLL_UP           (0x03)
#define CMD_SCROLL_DOWN         (0x04)
#define CMD_DBL_PRESS_LEFT      (0x05)
#define CMD_PRESS_LEFT          (0x06)
#define CMD_PRESS_RIGHT         (0x07)
#define CMD_MOVE                (0x08)

char buffer[0x1000 + 1]; // +1 for a nul-terminator

void I2C_RxHandler(int numBytes)
{
  // read command buffer
  byte RxByte = 0;
  size_t count = 0;
  size_t i = 0;
  uint8_t amount = 0;
  while(Wire.available()) {  // Read Any Received Data
    RxByte = Wire.read();
    if ( count < sizeof(buffer) )
    {
      buffer[count] = (char)RxByte;
      count++;
    }
  }

  // dispatch commands
  if ( count > 0 )
  {
    switch (buffer[0])
    {
      case CMD_TINY_USB_HID_KEY:
        for ( i = 1; i < count; i++ )
        {
          device.sendKey(buffer[i]);
        }
        break;

      case CMD_TYPE_STRING:
        if ( count > 1 )
        {
          buffer[count] = '\0';
          device.sendString(&buffer[1]);
        }
        break;

      case CMD_SCROLL_UP:
        amount = 1;
        if ( count > 1 )
        {
          amount = (uint8_t)buffer[1];
        }
        device.scrollUp(amount);
        break;

      case CMD_SCROLL_DOWN:
        amount = 1;
        if ( count > 1 )
        {
          amount = (uint8_t)buffer[1];
        }
        device.scrollDown(amount);
        break;

      case CMD_DBL_PRESS_LEFT:
        device.doublePressLeft();
        break;

      case CMD_PRESS_LEFT:
        device.pressLeft();
        break;

      case CMD_PRESS_RIGHT:
        device.pressRight();
        break;

      case CMD_MOVE:
        if ( count > 2 )
        {
          device.move((int8_t)buffer[1], (int8_t)buffer[2]);
        }
        break;

      default:
        break;
    }
  }
}

void setup()
{
    device.begin();
    Wire.begin(0x55);
    Wire.onReceive(I2C_RxHandler);
}

void loop()
{
    delay(1);
}

#endif
