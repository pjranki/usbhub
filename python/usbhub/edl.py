import os
import sys
import subprocess
import serial.tools.list_ports


class Edl(object):

    def __init__(self):
        pass

    def Popen(self, args, *_args, **_kwargs):
        new_args = [self._bin]
        new_args.extend(args)
        args = new_args
        return self._Popen(args, *_args, **_kwargs)

    def devices(self):
        device_list = list()
        for port, desc, hwid in serial.tools.list_ports.comports():
            if 'qdloader' in desc.lower().split(' '):
                device_list.append((port, desc))
        return device_list
