import os
import sys
import hid
import time
import serial
import serial.tools.list_ports
import usbhub
import argparse


def do_nothing(args):
    return


def do_usb_off(args):
    args.usb_5v.off()
    args.otg_5v.off()
    args.usb_cc.off()
    args.usb_cc_pc.off()
    args.usb_data.off()
    args.usb_data_pc.off()


def do_usb_charge(args):
    do_usb_off(args)
    time.sleep(0.1)
    args.usb_cc_pc.on()
    args.usb_cc.on()
    args.usb_5v.on()


def do_usb_pc(args):
    do_usb_off(args)
    time.sleep(0.1)
    args.usb_cc_pc.on()
    args.usb_data_pc.on()
    args.usb_data.on()
    args.usb_cc.on()
    args.usb_5v.on()


def do_usb_otg(args):
    do_usb_off(args)
    time.sleep(1)
    args.usb_cc_pc.off()    # OTG
    args.usb_data_pc.off()  # OTG
    args.usb_data.on()
    args.usb_cc.on()
    args.otg_5v.on()


def do_usb_keyboard_and_mouse(args):
    do_usb_otg(args)
    time.sleep(1.5)
    i2c = args.i2c(id=0x55, freq=100000)
    args.keyboard = usbhub.Esp32s2HidComposite(i2c)
    args.mouse = usbhub.Esp32s2HidComposite(i2c)
    args.mouse.mouse_width = 360
    args.mouse.mouse_height = 760
    args.mouse.pixel_width = 100
    args.mouse.pixel_height = 100


def do_battery_off(args):
    args.battery.off()


def do_battery_on(args):
    args.battery.on()


def do_power_off(args):
    do_usb_off(args)
    do_battery_off(args)
    time.sleep(1)
    do_battery_on(args)


def do_power_off(args):
    do_usb_off(args)
    do_battery_off(args)
    time.sleep(1)
    do_battery_on(args)


def do_charge(args):
    do_power_off(args)
    do_usb_charge(args)


def do_reboot(args):
    do_power_off(args)
    try:
        args.button_power.on()
        do_usb_pc(args)
        time.sleep(5)
    finally:
        args.button_power.off()


def do_edl(args):
    do_power_off(args)
    try:
        args.tp_edl.on()
        args.button_power.on()
        do_usb_pc(args)
        time.sleep(5)
    finally:
        args.button_power.off()
        args.tp_edl.off()


def do_recovery(args):
    do_power_off(args)
    try:
        args.button_vol_up.on()
        args.button_power.on()
        do_usb_pc(args)
        time.sleep(5)
    finally:
        args.button_power.off()
        args.button_vol_up.off()


def do_fastboot(args):
    do_power_off(args)
    try:
        args.button_vol_down.on()
        args.button_power.on()
        do_usb_pc(args)
        time.sleep(5)
    finally:
        args.button_power.off()
        args.button_vol_down.off()


def do_serial(args):
    if not getattr(args, 'serial', None):
        serial_port = None
        for port, desc, hwid in serial.tools.list_ports.comports():
            if hwid.lower().find('{:04x}:{:04x}'.format(args.vid, args.pid)) != -1:
                serial_port = port
                break
        if not serial_port:
            raise Exception('Serial port for MCP2221A not found')
        args.serial = usbhub.Serial(serial_port, args.baud)
    assert args.serial
    if args.func == do_serial:
        stdout = args.serial.shell(args.args)
        if len(args.args) > 0:
            print(stdout.decode().strip())


def do_unlock(args):
    try:
        do_usb_keyboard_and_mouse(args)
        args.keyboard.send_tiny_usb_hid_key(usbhub.TinyUSB.HID_KEY_ESCAPE)
        do_serial(args)
        adb = usbhub.Adb(Popen=args.serial)
        adb.unlock()
    finally:
        time.sleep(0.1)
        do_usb_pc(args)


def do_lock(args):
    do_usb_keyboard_and_mouse(args)
    args.keyboard.send_tiny_usb_hid_key(usbhub.TinyUSB.HID_KEY_ESCAPE)
    do_usb_pc(args)
    time.sleep(1)
    try:
        args.button_power.on()
        time.sleep(0.05)
    finally:
        args.button_power.off()


def do_back(args):
    do_serial(args)
    adb = usbhub.Adb(Popen=args.serial)
    adb.back()


def do_home(args):
    do_serial(args)
    adb = usbhub.Adb(Popen=args.serial)
    adb.home()


def do_click(args):
    try:
        do_usb_keyboard_and_mouse(args)
        args.mouse.pos(int(args.x), int(args.y))
        args.mouse.press_left()
    finally:
        time.sleep(0.1)
        do_usb_pc(args)


def do_type(args):
    try:
        do_usb_keyboard_and_mouse(args)
        args.keyboard.send_string(' '.join(args.args))
    finally:
        time.sleep(0.1)
        do_usb_pc(args)


def do_keypress(args):
    key = args.key
    key_map = dict()
    for name in dir(usbhub.TinyUSB):
        if name.startswith('HID_KEY_'):
            key_map[name[len('HID_KEY_'):].lower()] = getattr(usbhub.TinyUSB, name)
    if key not in key_map:
        print('Invalid key: "{}"'.format(key))
        print(list(key_map.keys()))
        return
    try:
        do_usb_keyboard_and_mouse(args)
        args.keyboard.send_tiny_usb_hid_key(key_map[key])
    finally:
        time.sleep(0.1)
        do_usb_pc(args)


def do_backspace(args):
    try:
        do_usb_keyboard_and_mouse(args)
        for _ in range(0, 100):
            args.keyboard.send_string('\x08' * 50)
    finally:
        time.sleep(0.1)
        do_usb_pc(args)


def do_adb_wait(args):
    adb = usbhub.Adb()
    while len(adb.devices()) == 0:
        time.sleep(1)


def do_android_wait(args):
    do_adb_wait(args)
    adb = usbhub.Adb()
    while not adb.boot_completed:
        time.sleep(1)


def do_fastboot_wait(args):
    fastboot = usbhub.Fastboot()
    while len(fastboot.devices()) == 0:
        time.sleep(1)


def do_edl_wait(args):
    edl = usbhub.Edl()
    while len(edl.devices()) == 0:
        time.sleep(1)


def do_edl_devices(args):
    edl = usbhub.Edl()
    for port, desc in edl.devices():
        print('{} {}'.format(port.ljust(30), desc))


def do_button_power(args):
    try:
        args.button_power.on()
        time.sleep(0.05)
    finally:
        args.button_power.off()


def do_button_vol_up(args):
    try:
        args.button_vol_up.on()
        time.sleep(0.05)
    finally:
        args.button_vol_up.off()


def do_button_vol_down(args):
    try:
        args.button_vol_down.on()
        time.sleep(0.05)
    finally:
        args.button_vol_down.off()


def main():
    hid_device = None
    try:
        parser = argparse.ArgumentParser()
        parser.add_argument('--vid', default='0x{:04x}'.format(usbhub.MCP2221A.VID), 
            help="USB VID for device actuator (default: MCP2221A USB VID 0x{:04x})".format(usbhub.MCP2221A.VID))
        parser.add_argument('--pid', default='0x{:04x}'.format(usbhub.MCP2221A.PID), 
            help="USB PID for device actuator (default: MCP2221A USB PID 0x{:04x})".format(usbhub.MCP2221A.PID))
        parser.add_argument('--baud', default=115200, 
            help="USB serial Baud Rate for device actuator (default: {})".format(115200))
        subparsers = parser.add_subparsers()
        parser.set_defaults(func=do_nothing)
        subparser = subparsers.add_parser('poweroff', help='Power off device by disconnecting batter and USB power briefly')
        subparser.set_defaults(func=do_power_off)
        subparser = subparsers.add_parser('charge', help='Power off device and connect USB for power only')
        subparser.set_defaults(func=do_charge)
        subparser = subparsers.add_parser('reboot', help='Reboot phone into Android')
        subparser.set_defaults(func=do_reboot)
        subparser = subparsers.add_parser('recovery', help='Reboot phone into Recovery mode')
        subparser.set_defaults(func=do_recovery)
        subparser = subparsers.add_parser('fastboot', help='Reboot phone into Fastboot mode')
        subparser.set_defaults(func=do_fastboot)
        subparser = subparsers.add_parser('edl', help='Reboot phone into EDL mode')
        subparser.set_defaults(func=do_edl)
        subparser = subparsers.add_parser('unlock', help='Unlock phone screen')
        subparser.set_defaults(func=do_unlock)
        subparser = subparsers.add_parser('lock', help='Lock and turn off phone screen')
        subparser.set_defaults(func=do_lock)
        subparser = subparsers.add_parser('back', help='Back button (via shell/adb)')
        subparser.set_defaults(func=do_back)
        subparser = subparsers.add_parser('home', help='Home button (via shell/adb)')
        subparser.set_defaults(func=do_home)
        subparser = subparsers.add_parser('serial', help='Open a serial terminal to phone or run serial command')
        subparser.set_defaults(func=do_serial)
        subparser = subparsers.add_parser('click', help='Click a part of the screen using a usb mouse')
        subparser.set_defaults(func=do_click)
        subparser.add_argument('x', help='X position')
        subparser.add_argument('y', help='y position')
        subparser = subparsers.add_parser('type', help='Type text onto phone using usb keyboard')
        subparser.set_defaults(func=do_type)
        subparser = subparsers.add_parser('keypress', help='Press a key on the usb keyboard')
        subparser.add_argument('key', help='Key to press position')
        subparser.set_defaults(func=do_keypress)
        subparser = subparsers.add_parser('backspace', help='Press backspace a bunch of times on the usb keyboard')
        subparser.set_defaults(func=do_backspace)
        subparser = subparsers.add_parser('adb_wait', help='Wait for any adb device')
        subparser.set_defaults(func=do_adb_wait)
        subparser = subparsers.add_parser('android_wait', help='Wait for any android to boot to lockscree/homescreen')
        subparser.set_defaults(func=do_android_wait)
        subparser = subparsers.add_parser('fastboot_wait', help='Wait for any fastboot device')
        subparser.set_defaults(func=do_fastboot_wait)
        subparser = subparsers.add_parser('edl_wait', help='Wait for any EDL device')
        subparser.set_defaults(func=do_edl_wait)
        subparser = subparsers.add_parser('edl_devices', help='List EDL devices')
        subparser.set_defaults(func=do_edl_devices)
        subparser = subparsers.add_parser('usb_off', help='Turn USB off')
        subparser.set_defaults(func=do_usb_off)
        subparser = subparsers.add_parser('usb_charge', help='Turn USB on for power only')
        subparser.set_defaults(func=do_usb_charge)
        subparser = subparsers.add_parser('usb_pc', help='Turn USB on for power and data connection')
        subparser.set_defaults(func=do_usb_pc)
        subparser = subparsers.add_parser('usb_otg', help='Turn USB for OTG connection')
        subparser.set_defaults(func=do_usb_otg)
        subparser = subparsers.add_parser('button_power', help='Tap the POWER button on device')
        subparser.set_defaults(func=do_button_power)
        subparser = subparsers.add_parser('button_vol_up', help='Tap the VOL+ button on device')
        subparser.set_defaults(func=do_button_vol_up)
        subparser = subparsers.add_parser('button_vol_down', help='Tap the VOL- button on device')
        subparser.set_defaults(func=do_button_vol_down)

        # parse
        args = sys.argv[1:]
        sub_args = list()
        index = 0
        for arg in args:
            if arg in ['serial', 'type']:
                break
            index += 1
        if index != len(args):
            sub_args = args[index + 1:]
            args = args[:index + 1]
        args = parser.parse_args(args)
        args.args = sub_args
        args.vid = int(args.vid, 0)
        args.pid = int(args.pid, 0)

        # open device
        hid_device = hid.device()
        hid_device.open_path(hid.enumerate(args.vid, args.pid)[0]["path"])

        # setup USB/HID chip on device actuator
        mcp2221a = usbhub.MCP2221A(hid_device)

        # control pins
        args.i2c = mcp2221a.I2C
        i2c = mcp2221a.I2C(id=0x20, freq=100000)
        mcp23017 = usbhub.MCP23017(i2c)
        args.otg_5v = mcp23017.PinB(0, usbhub.MCP23017Pin.OUT)
        args.usb_5v = usbhub.ActiveLow(mcp23017.PinB(1, usbhub.MCP23017Pin.OUT))
        args.usb_cc_pc = usbhub.ActiveLow(mcp23017.PinB(2, usbhub.MCP23017Pin.OUT))
        args.usb_cc = usbhub.ActiveLow(mcp23017.PinB(3, usbhub.MCP23017Pin.OUT))
        args.usb_data_pc = usbhub.ActiveLow(mcp23017.PinB(4, usbhub.MCP23017Pin.OUT))
        args.usb_data = usbhub.ActiveLow(mcp23017.PinB(5, usbhub.MCP23017Pin.OUT))
        args.battery = usbhub.ActiveLow(mcp23017.PinB(6, usbhub.MCP23017Pin.OUT))
        args.tp_edl = mcp23017.PinB(7, usbhub.MCP23017Pin.OUT) # using 2nd relay instead of tri-state
        args.button_power = mcp23017.PinA(1, usbhub.MCP23017Pin.OUT)
        args.button_vol_up = mcp23017.PinA(2, usbhub.MCP23017Pin.OUT)
        args.button_vol_down = mcp23017.PinA(3, usbhub.MCP23017Pin.OUT)

        # do stuff
        args.func(args)
    finally:
        if hid_device:
            del hid_device
            hid_device = None

if __name__ == '__main__':
    main()
