import os
import sys
import time


class Xdcp(object):

    def __init__(self, r_kohm, tap_points, cs=None, ud=None, inc=None):
        self.r_kohm = r_kohm
        self.tap_points = tap_points
        self.tap_point = None
        self.cs = cs
        self.ud = ud
        self.inc = inc
        self.set_low()

    def move_wiper(self, count):
        self.inc.on()
        self.cs.off()
        for _ in range(0, count):
            self.inc.off()
            self.inc.on()
        self.cs.on()

    def increment(self, count):
        assert self.tap_point >= 0 and self.tap_point < self.tap_points
        if self.tap_point + count >= self.tap_points:
            count = (self.tap_points - 1) - self.tap_point
        self.ud.on()
        self.move_wiper(count)
        self.ud.on()
        self.tap_point += count
        assert self.tap_point >= 0 and self.tap_point < self.tap_points

    def decrement(self, count):
        assert self.tap_point >= 0 and self.tap_point < self.tap_points
        if self.tap_point - count < 0:
            count = self.tap_point
        self.ud.off()
        self.move_wiper(count)
        self.ud.on()
        self.tap_point -= count
        assert self.tap_point >= 0 and self.tap_point < self.tap_points

    def set_low(self):
        self.tap_point = (self.tap_points - 1)
        self.decrement(self.tap_point)
        assert self.tap_point == 0

    def set_high(self):
        self.tap_point = 0
        self.increment(self.tap_points - 1)
        assert self.tap_point == self.tap_points - 1


class X9C102(Xdcp):

    def __init__(self, *args, **kwargs):
        super().__init__(1, 100, *args, **kwargs)

class X9C103(Xdcp):

    def __init__(self, *args, **kwargs):
        super().__init__(10, 100, *args, **kwargs)

class X9C503(Xdcp):

    def __init__(self, *args, **kwargs):
        super().__init__(50, 100, *args, **kwargs)

class X9C104(Xdcp):

    def __init__(self, *args, **kwargs):
        super().__init__(100, 100, *args, **kwargs)
