import os
import sys
import time
import struct


class MCP2221APin(object):
    UNKNOWN = 0
    IN = 1
    OUT = 2

    def __init__(self, mcp2221a, id, mode=-1, value=None):
        assert id >= 0
        assert id <= 3
        self.mcp2221a = mcp2221a
        self.id = id
        self.mode(mode)
        self.value(value)

    def mode(self, mode=None):
        if mode == -1:
            self.mcp2221a.write(b'\x00\x61')
            resp = self.mcp2221a.read()
            resp_bit = (resp[(22 + self.id) + 1] >> 4) & 0x01  # 1:Hi 0:LOW
            resp_dir = (resp[(22 + self.id) + 1] >> 3) & 0x01  # 0:OutPut 1:Input
            resp_mode = resp[(22 + self.id) + 1] & 0x07  # GPIO MODE = 0x00
            if resp_dir == 0:
                mode = self.OUT
            elif resp_dir == 1:
                mode = self.IN
            else:
                raise NotImplementedError()
        if mode is not None:
            if mode == self.OUT:
                direction = 0
            elif mode == self.IN:
                direction = 1
            else:
                raise NotImplementedError()
            req = b'\x00\x50'
            offset = (self.id + 1) * 4
            req = req.ljust(offset, b'\x00')
            req += struct.pack('<B', 0x01)      # set pin direction
            req += struct.pack('<B', direction) # to this
            self.mcp2221a.write(req)
            resp = self.mcp2221a.read()
            if resp[1] != 0x00:
                raise Exception("set mode error")
        self.mcp2221a.write(b'\x00\x61')
        resp = self.mcp2221a.read()
        resp_bit = (resp[(22 + self.id) + 1] >> 4) & 0x01  # 1:Hi 0:LOW
        resp_dir = (resp[(22 + self.id) + 1] >> 3) & 0x01  # 0:OutPut 1:Input
        resp_mode = resp[(22 + self.id) + 1] & 0x07  # GPIO MODE = 0x00
        if resp_dir == 0:
            mode = self.OUT
        elif resp_dir == 1:
            mode = self.IN
        else:
            raise NotImplementedError()
        return mode

    def value(self, value=None):
        if value is not None:
            req = b'\x00\x50'
            offset = ((self.id + 1) * 4) - 1
            req = req.ljust(offset, b'\x00')
            req += struct.pack('<B', 0x01)  # set pin value
            req += struct.pack('<B', value) # to this
            self.mcp2221a.write(req)
            resp = self.mcp2221a.read()
            if resp[1] != 0x00:
                raise Exception("set value error")
        self.mcp2221a.write(b'\x00\x51')
        resp = self.mcp2221a.read()
        bit = resp[(self.id + 1) * 2]
        if value is not None:
            assert value == bit

    def on(self):
        self.value(1)

    def off(self):
        self.value(0)


class MCP2221AI2CAddressNotFoundError(Exception):
    pass


class MCP2221AI2C(object):

    def __init__(self, mcp2221a, id=None, *args, **kwargs):
        self.mcp2221a = mcp2221a
        self.id = id
        if self.id is not None:
            self.readfrom(self.id, 1)

    def read(self, nbytes):
        assert self.id is not None
        return self.readfrom(self.id, nbytes)

    def write(self, buf):
        assert self.id is not None
        return self.writeto(self.id, buf)

    def readfrom(self, addr, nbytes):
        req = b'\x00\x91'
        req += struct.pack('<H', nbytes)
        req += struct.pack('<B', addr << 1)
        self.mcp2221a.write(req)
        resp = self.mcp2221a.read()
        if resp[1] != 0x00:
            self.mcp2221a.i2c_cancel()
            self.mcp2221a.i2c_init()
            raise Exception("I2C read error (1): 0x{:02x}".format(resp[1]))
        self.mcp2221a.i2c_sleep()
        req = b'\x00\x40'
        req += b'\x00\x00\x00'
        self.mcp2221a.write(req)
        resp = self.mcp2221a.read()
        if resp[1] != 0x00:
            self.mcp2221a.i2c_cancel()
            self.mcp2221a.i2c_init()
            if resp[1] == 0x41:
                raise MCP2221AI2CAddressNotFoundError(hex(addr))
            raise Exception("I2C read error (2): 0x{:02x}".format(resp[1]))
        if resp[2] == 0x00 and resp[3] == 0x00:
            self.mcp2221a.i2c_cancel()
            self.mcp2221a.i2c_init()
            return resp[4]
        if resp[2] != 0x55 or resp[3] != nbytes:
            raise Exception("I2C read error (3)")
        data = resp[4: 4 + nbytes]
        assert len(data) == nbytes
        return data
 
    def writeto(self, addr, buf):
        req = b'\x00\x90'
        req += struct.pack('<H', len(buf))
        req += struct.pack('<B', addr << 1)
        req += buf
        self.mcp2221a.write(req)
        resp = self.mcp2221a.read()
        time.sleep(0.008)
        return 1

    def scan(self):
        found = list()
        for addr in range(0x00,0x7F):
            try:
                self.readfrom(addr, 1)
                found.append(addr)
            except MCP2221AI2CAddressNotFoundError:
                pass
        return found


class MCP2221A(object):
    VID = 0x04D8
    PID = 0x00DD
    PACKET_SIZE = 65

    def __init__(self, hid=None):
        self.hid = hid
        self.gpio_ready = False
        self.i2c_ready = False
        self.write(b'\x00\x60')
        resp = self.read(2)
        assert resp == b'\x60\x00'

    def read(self, size=PACKET_SIZE):
        assert size <= self.PACKET_SIZE
        data = self.hid.read(self.PACKET_SIZE)
        if size == self.PACKET_SIZE:
            size -= 1
        data = b''.join(struct.pack('<B', data[i]) for i in range(0, len(data)))
        assert len(data) >= size
        return data[:size]

    def write(self, data):
        assert len(data) <= self.PACKET_SIZE
        size = len(data)
        data = [struct.unpack('<B', data[i: i + 1])[0] for i in range(0, len(data))]
        data = data + [0 for i in range(self.PACKET_SIZE - len(data))]
        self.hid.write(data)
        return size

    def gpio_init(self):
        self.write(b'\x00\x61')
        resp = self.read()
        req = b'\x00\x60\x00'
        req += struct.pack('<B', resp[5])   # Clock Output Divider value
        req += struct.pack('<B', resp[6])   # DAC Voltage Reference
        req += struct.pack('<B', 0x00)      # Set DAC output value
        req += struct.pack('<B', 0x00)      # ADC Voltage Reference
        req += struct.pack('<B', 0x00)      # Setup the interrupt detection mechanism and clear the detection flag
        req += struct.pack('<B', 0x80)      # Alter GPIO configuration: alters the current GP designation
        #   datasheet says this should be 1, but should actually be 0x80
        self.write(req)
        resp = self.read()
        self.gpio_ready = True

    def i2c_init(self, freq=400000):
        req = b'\x00\x10'
        req += b'\x00'
        req += struct.pack('<B', 0x00)  # Cancel current I2C/SMBus transfer (sub-command)
        req += struct.pack('<B', 0x20)  # Set I2C/SMBus communication speed (sub-command)
        # The I2C/SMBus system clock divider that will be used to establish the communication speed
        req += struct.pack('<B', round(12000000 / freq) - 2)
        self.write(req)
        resp = self.read()
        if resp[3] != 0x20:
            raise Exception("I2C speed is not valid or bus is busy")
        if resp[22] == 0:
            raise Exception("SCL is low")
        if resp[23] == 0:
            raise Exception("SDA is low")
        self.i2c_ready = True

    def i2c_sleep(self):
        time.sleep(0)

    def i2c_cancel(self):
        req = b'\x00\x10'
        req += b'\x00'
        req += struct.pack('<B', 0x10)  # Cancel current I2C/SMBus transfer (sub-command)
        self.write(req)
        self.read()

    def Pin(self, *args, **kwargs):
        if not self.gpio_ready:
            self.gpio_init()
        return MCP2221APin(self, *args, **kwargs)

    def I2C(self, *args, **kwargs):
        if not self.i2c_ready:
            self.i2c_init(kwargs.get('freq', 400000))
        return MCP2221AI2C(self, *args, **kwargs)
