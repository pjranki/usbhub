import os
import sys
import subprocess


class Fastboot(object):

    def __init__(self, bin=None, Popen=None):
        self._bin = bin if bin else ('fastboot.exe' if os.name == 'nt' else 'fastboot')
        self._Popen = Popen if Popen else subprocess.Popen

    def Popen(self, args, *_args, **_kwargs):
        new_args = [self._bin]
        new_args.extend(args)
        args = new_args
        return self._Popen(args, *_args, **_kwargs)

    def devices(self):
        args = [self._bin, 'devices']
        p = self._Popen(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = p.communicate()
        device_list = list()
        for line in stdout.decode().split('\n'):
            line = line.strip()
            line = line.replace('\t', ' ')
            while line.find('  ') != -1:
                line = line.replace('  ', ' ')
            items = line.split(' ')
            if len(items) >= 2:
                device_list.append((items[0], items[1]))
        return device_list
