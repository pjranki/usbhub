# USB Hub
This repo contains multiple hardware projects designed around device automation.

## Device Actuator (previously usbhub)
![Device Actuator v1](device_actuator_v1.png)

## Initial idea parts list
 - MAX1562 (USB power switch)
 - FE1.1s (USB 2.0 hub)
 - TS3USB221 (USB multiplexer)
 - Digital potentiometer (for 4.7k, 5.1k, 56k, 10k - USB-C device control).
 - USB-C breakouts boards for prototyping.
 - CBTL08GP053 (USB 3.1 and DisplayPort multiplexer).
 - MCP2221 (USB 2.0 HID to GPIO/I2C/UART).
 - 3.3V uart to 1.8V uart step down.

## Python deps
```
sudo apt install libhidapi-dev
python3 -m pip install hid
```

## USB MCP2221A udev
```
# /etc/udev/rules.d/50-MCP2221A.rules
SUBSYSTEM=="usb",ATTRS{idVendor}=="04D8",ATTRS{idProduct}=="00DD",MODE="0666"
```
