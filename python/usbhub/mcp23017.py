import os
import sys
import time
import struct


class MCP23017Pin(object):
    PORTA = 0
    PORTB = 1
    UNKNOWN = 0
    IN = 1
    OUT = 2
    IODIRA = 0x00
    IODIRB = 0x01
    GPIOA = 0x12
    GPIOB = 0x13

    def __init__(self, mcp23017, port, id, mode=-1, value=None):
        assert id >= 0
        assert id <= 7
        self.mcp23017 = mcp23017
        self.port = port
        assert self.port == self.PORTA or self.port == self.PORTB
        self.id = id
        self.mode(mode)
        self.value(value)
 
    def mode(self, mode=None):
        reg = self.IODIRA if self.port == self.PORTA else self.IODIRB
        if mode == -1:
            direction = (self.mcp23017.readfrom(reg) >> self.id) & 0x1
            if direction == 0:
                mode = self.OUT
            elif direction == 1:
                mode = self.IN
            else:
                raise NotImplementedError()
        if mode is not None:
            if mode == self.OUT:
                direction = 0
            elif mode == self.IN:
                direction = 1
            else:
                raise NotImplementedError()
            new_value = self.mcp23017.readfrom(reg)
            new_value &= (~(1 << self.id) & 0xff)
            new_value |= (direction & 0x1) << self.id
            self.mcp23017.writeto(reg, new_value)
        direction = (self.mcp23017.readfrom(reg) >> self.id) & 0x1
        if direction == 0:
            mode = self.OUT
        elif direction == 1:
            mode = self.IN
        else:
            raise NotImplementedError()
        return mode

    def value(self, value=None):
        reg = self.GPIOA if self.port == self.PORTA else self.GPIOB
        if value is not None:
            new_value = self.mcp23017.readfrom(reg)
            new_value &= (~(1 << self.id) & 0xff)
            new_value |= (value & 0x1) << self.id
            self.mcp23017.writeto(reg, new_value)
        return (self.mcp23017.readfrom(reg) >> self.id) & 0x1

    def on(self):
        self.value(1)

    def off(self):
        self.value(0)


class MCP23017(object):

    def __init__(self, i2c):
        self.i2c = i2c
        self.gpio_ready = False

    def read(self, nbytes):
        return self.i2c.read(nbytes)

    def write(self, buf):
        return self.i2c.write(buf)

    def writeto(self, reg, value):
        req = struct.pack('<BB', reg, value)
        self.write(req)

    def readfrom(self, reg):
        req = struct.pack('<B', reg)
        self.write(req)
        time.sleep(0)
        return struct.unpack('<B', self.read(1))[0]

    def gpio_init(self):
        self.gpio_ready = True

    def PinA(self, *args, **kwargs):
        if not self.gpio_ready:
            self.gpio_init()
        return MCP23017Pin(self, MCP23017Pin.PORTA, *args, **kwargs)

    def PinB(self, *args, **kwargs):
        if not self.gpio_ready:
            self.gpio_init()
        return MCP23017Pin(self, MCP23017Pin.PORTB, *args, **kwargs)
