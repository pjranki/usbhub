import os
import sys
import time
import struct


class ActiveLow(object):

    def __init__(self, pin):
        self.pin = pin

    def value(self, value=None):
        raise NotImplementedError

    def on(self):
        self.pin.off()

    def off(self):
        self.pin.on()
