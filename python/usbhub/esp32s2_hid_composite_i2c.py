import struct


class Esp32s2HidComposite(object):
    CMD_TINY_USB_HID_KEY    = 0x01
    CMD_TINY_TYPE_STRING    = 0x02
    CMD_SCROLL_UP           = 0x03
    CMD_SCROLL_DOWN         = 0x04
    CMD_DBL_PRESS_LEFT      = 0x05
    CMD_PRESS_LEFT          = 0x06
    CMD_PRESS_RIGHT         = 0x07
    CMD_MOVE                = 0x08

    def __init__(self, i2c):
        self.i2c = i2c
        self.mouse_width = 0
        self.mouse_height = 0
        self.pixel_width = 0
        self.pixel_height = 0

    def send_tiny_usb_hid_key(self, key):
        self.i2c.write(struct.pack('<BB', self.CMD_TINY_USB_HID_KEY, key))

    def send_string(self, text):
        if isinstance(text, str):
            data = text.encode('ascii')
        else:
            data = text
        assert isinstance(data, bytes)
        self.i2c.write(struct.pack('<B', self.CMD_TINY_TYPE_STRING) + data)

    def scroll_up(self, amount=1):
        self.i2c.write(struct.pack('<BB', self.CMD_SCROLL_UP, amount))

    def scroll_down(self, amount=1):
        self.i2c.write(struct.pack('<BB', self.CMD_SCROLL_DOWN, amount))

    def double_press_left(self):
        self.i2c.write(struct.pack('<B', self.CMD_DBL_PRESS_LEFT))

    def press_left(self):
        self.i2c.write(struct.pack('<B', self.CMD_PRESS_LEFT))

    def press_right(self):
        self.i2c.write(struct.pack('<B', self.CMD_PRESS_RIGHT))

    def move(self, x, y):
        while True:
            x_adjust = 0
            y_adjust = 0
            if x < -127:
                x_adjust = -127
            elif x > 127:
                x_adjust = 127
            if y < -127:
                y_adjust = -127
            elif y > 127:
                y_adjust = 127
            if x_adjust == 0 and y_adjust == 0:
                break
            self.i2c.write(struct.pack('<Bbb', self.CMD_MOVE, x_adjust, y_adjust))
            x -= x_adjust
            y -= y_adjust
        assert x >= -127 and x <= 127
        assert y >= -127 and y <= 127
        self.i2c.write(struct.pack('<Bbb', self.CMD_MOVE, x, y))

    def pos(self, x, y, ):
        self.move(-2 * self.mouse_width, -2 * self.mouse_height)
        mouse_x = int((x * self.mouse_width) / self.pixel_width)
        mouse_y = int((y * self.mouse_height) / self.pixel_height)
        self.move(mouse_x, mouse_y)
